package restclient.model;

import java.util.Objects;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Objekt poskytujuci nezavisle aplikacne rozhranie reprezentujuce objekt "Destinace"
 */
@XmlRootElement
public class DestinaceTO extends CommonTO {
    
    private String stat;
    private String mesto;

    public DestinaceTO() {
    }

    public DestinaceTO(String stat, String mesto) {
        this.stat = stat;
        this.mesto = mesto;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public String getMesto() {
        return mesto;
    }

    public void setMesto(String mesto) {
        this.mesto = mesto;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.stat);
        hash = 43 * hash + Objects.hashCode(this.mesto);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DestinaceTO other = (DestinaceTO) obj;
        if (!Objects.equals(this.stat, other.stat)) {
            return false;
        }
        if (!Objects.equals(this.mesto, other.mesto)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DestinaceTO{" + "stat='" + stat + "', mesto='" + mesto + "'}";
    }
}
