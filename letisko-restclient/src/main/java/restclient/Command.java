package restclient;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.filter.HttpBasicAuthFilter;
import restclient.model.CommonTO;
import restclient.model.DestinaceTO;
import restclient.model.LetuskaTO;

/**
 *
 * @author Martin Pitonak <mpitonak@mail.muni.cz>
 */
public abstract class Command {

    protected final String URL_LETISKO = "http://localhost:8080/letisko-rest/letisko";

    /**
     *
     * @param params
     * @return vrati false ak sa ma ukoncit aplikacia, inac true
     */
    public abstract boolean execute(String params);

    protected List<? extends CommonTO> showList(Class<? extends CommonTO> c) {        
        Client client = ClientBuilder.newClient();
        Response response = client.target(URL_LETISKO).path(c == LetuskaTO.class ? "letuska" : "destinace")
                .register(new HttpBasicAuthFilter("admin", "admin")).request(MediaType.APPLICATION_XML).get();
        client.close();
        if (response.getStatus() == 200) {
            if (c == LetuskaTO.class) {
                List<LetuskaTO> letusky = new ArrayList<LetuskaTO>(response.readEntity(new GenericType<Set<LetuskaTO>>() {}));
                if (!letusky.isEmpty()) {
                    System.out.println("\nList of flight attendants:");
                    for (int i = 0; i < letusky.size(); i++) {
                        System.out.println(i + ". " + letusky.get(i).getMeno() + " " + letusky.get(i).getPriezvisko());
                    }
                } else {
                    System.out.println("\nEmpty list.");
                }
                return letusky;
            } else {
                List<DestinaceTO> destinacie = new ArrayList<DestinaceTO>(response.readEntity(new GenericType<Set<DestinaceTO>>() {}));
                if (!destinacie.isEmpty()) {
                    System.out.println("\nList of destinations:");
                    for (int i = 0; i < destinacie.size(); i++) {
                        System.out.println(i + ". " + destinacie.get(i).getStat() + ", " + destinacie.get(i).getMesto());
                    }
                } else {
                    System.out.println("\nEmpty list.");
                }
                return destinacie;
            }
        } else {
            System.out.println("\nError: " + response.getStatusInfo().getReasonPhrase());
            return null;
        }
    }
}
