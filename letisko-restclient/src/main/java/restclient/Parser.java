
package restclient;

import java.util.ArrayList;
import java.util.List;
import restclient.commands.*;

/**
 *
 * @author Martin Pitonak <mpitonak@mail.muni.cz>
 */
public class Parser {

    private List<Command> commands;
    
    public Parser(){
        commands = new ArrayList<Command>(6);
        commands.add(new Quit());
        commands.add(new Help());
        commands.add(new Show());
        commands.add(new Add());
        commands.add(new Edit());
        commands.add(new Delete());
    }
    
    public Command getCommand(String arg) {
        if (arg.isEmpty()) return null;
        if (arg.matches("exit")) return commands.get(0);
        if (arg.matches("help")) return commands.get(1);
        if (arg.matches("show [fd]")) return commands.get(2);
        if (arg.matches("add [fd] [a-zA-Z]+ [a-zA-Z]+")) return commands.get(3);
        if (arg.matches("edit [fd]")) return commands.get(4);
        if (arg.matches("delete [fd]")) return commands.get(5);
        return null;
    }
}
