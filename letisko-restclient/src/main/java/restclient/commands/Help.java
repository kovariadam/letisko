package restclient.commands;

import restclient.Command;

/**
 *
 * @author Martin Pitonak <mpitonak@mail.muni.cz>
 */
public class Help extends Command {

    @Override
    public boolean execute(String params) {
        System.out.println("\nLetisko client");
        System.out.println("usage:");
        System.out.println("    show (F | D)                  - shows all flight attendants / destinations");
        System.out.println("    edit (F | D)                  - edits flight attendant / destination");
        System.out.println("    add F <firstname> <surname>   - adds flight attendant");
        System.out.println("    add D <country> <city>        - adds destination");
        System.out.println("    delete (F | D)                - deletes flight attendant / destination");
        System.out.println("    help                          - shows this help");
        System.out.println("    exit                          - exits application");
        return true;
    }
}
