package restclient.commands;

import restclient.Command;
import restclient.model.DestinaceTO;
import restclient.model.LetuskaTO;

/**
 *
 * @author Martin Pitonak <mpitonak@mail.muni.cz>
 */
public class Show extends Command {

    @Override
    public boolean execute(String arg) {
        if (arg.endsWith("f") || arg.endsWith("F") ){
            showList(LetuskaTO.class);
        }else{
            showList(DestinaceTO.class);
        }
        return true;
    }
}
