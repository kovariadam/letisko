package restclient.commands;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.filter.HttpBasicAuthFilter;
import restclient.Command;
import restclient.model.CommonTO;
import restclient.model.DestinaceTO;
import restclient.model.LetuskaTO;

/**
 *
 * @author Martin Pitonak <mpitonak@mail.muni.cz>
 */
public class Edit extends Command {

    @Override
    public boolean execute(String arg) {
        boolean isLetuska = arg.endsWith("f") | arg.endsWith("F");
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(URL_LETISKO).path(isLetuska ? "letuska" : "destinace");
        List<? extends CommonTO> list = showList(isLetuska ? LetuskaTO.class : DestinaceTO.class);
        if (list == null || list.isEmpty()) return true;

        System.out.print("Enter number of row to edit: ");
        Scanner br = new Scanner(System.in);
        int id;
        try {
            id = br.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Error: Invalid character.");
            return true;
        }
        if (id >= list.size() || id < 0) {
            System.out.println("Error: Number out of bounds.");
            return true;
        }
        System.out.print("Enter " + (isLetuska ? "first name: " : "country: "));
        String param1 = br.next();
        System.out.print("Enter " + (isLetuska ? "last name: " : "city: "));
        String param2 = br.next();
        Response response;
        if (isLetuska) {
            LetuskaTO letuska = ((LetuskaTO) list.get(id));
            letuska.setMeno(param1);
            letuska.setPriezvisko(param2);
            response = target.register(new HttpBasicAuthFilter("admin", "admin")).request().put(Entity.xml(letuska));
        } else {
            DestinaceTO destinace = ((DestinaceTO) list.get(id));
            destinace.setStat(param1);
            destinace.setMesto(param2);
            response = target.register(new HttpBasicAuthFilter("admin", "admin")).request().put(Entity.xml(destinace));
        }
        System.out.println(response.getStatus() == 200 ? "Successfully modified." : "Could not modify.");
        client.close();
        return true;
    }

}
