package restclient.commands;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.filter.HttpBasicAuthFilter;
import restclient.Command;
import restclient.model.DestinaceTO;
import restclient.model.LetuskaTO;

/**
 *
 * @author Martin Pitonak <mpitonak@mail.muni.cz>
 */
public class Add extends Command {

    @Override
    public boolean execute(String params) {
        String[] words = params.split("\\s");
        String path;
        Entity entita;
        if (words[1].equalsIgnoreCase("f")) {
            path = "letuska";
            entita = Entity.xml(new LetuskaTO(words[2], words[3]));
        } else {
            path = "destinace";
            entita = Entity.xml(new DestinaceTO(words[2], words[3]));
        }
        Client client = ClientBuilder.newClient();
        Response response = client.target(URL_LETISKO).path(path).register(new HttpBasicAuthFilter("admin", "admin")).request().put(entita);
        System.out.println(response.getStatus() == 201 ? "\nSuccessfully created." : "\nCould not create.");
        client.close();
        return true;
    }
}
