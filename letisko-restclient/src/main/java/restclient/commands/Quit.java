
package restclient.commands;

import restclient.Command;

/**
 *
 * @author Martin Pitonak <mpitonak@mail.muni.cz>
 */
public class Quit extends Command{

    @Override
    public boolean execute(String params) {
        return false;
    }
    
}
