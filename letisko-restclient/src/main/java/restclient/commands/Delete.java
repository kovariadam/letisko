package restclient.commands;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.filter.HttpBasicAuthFilter;
import restclient.Command;
import restclient.model.CommonTO;
import restclient.model.DestinaceTO;
import restclient.model.LetuskaTO;

/**
 *
 * @author Martin Pitonak <mpitonak@mail.muni.cz>
 */
public class Delete extends Command {

    @Override
    public boolean execute(String arg) {
        boolean isLetuska = arg.endsWith("f") | arg.endsWith("F");
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(URL_LETISKO);
        List<? extends CommonTO> list = showList(isLetuska ? LetuskaTO.class : DestinaceTO.class);
        if (list == null || list.isEmpty()) return true;
        
        System.out.print("Enter number of row to delete: ");
        Scanner br = new Scanner(System.in);
        int id;
        try {
            id = br.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Error: Invalid character.");
            return true;
        }
        if (id >= list.size() || id < 0) {
            System.out.println("Error: Number out of bounds.");
            return true;
        }
        String path = isLetuska ? "letuska/" : "destinace/";
        path += String.valueOf(list.get(id).getId());
        Response response = target.path(path).register(new HttpBasicAuthFilter("admin", "admin")).request().delete();
        System.out.println(response.getStatus() == 204 ? "Successfully deleted." : "Could not delete.");
        client.close();
        return true;
    }

}
