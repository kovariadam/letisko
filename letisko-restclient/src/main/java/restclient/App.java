package restclient;

import java.net.ConnectException;
import java.util.Scanner;

public class App {

    private Parser parser;

    public App() {
        parser = new Parser();
    }

    public static void main(String[] args) {
        new App().start();
    }

    public void start() {
        System.out.println("Letisko client");
        System.out.println("for help with application type help");
        System.out.println("Enter command:");
        Scanner scanner = new Scanner(System.in);
        String line;
        do {
            System.out.print(">> ");
            line = scanner.nextLine().trim();
        } while (line.isEmpty() || processCommand(line));
    }

    private boolean processCommand(String line) {
        Command command = parser.getCommand(line.toLowerCase());
        boolean result = true;
        if (command == null) {
            System.out.println("Invalid command.");
            return true;
        } else {
            try {
                result = command.execute(line);
            } catch (Exception e) {
                if (e.getCause() instanceof ConnectException) {
                    System.out.println("\nError: Could not connect to server.");
                } else {
                    System.out.println("\nError: Unspecified error.");
                }
            }
        }
        return result;
    }

}
