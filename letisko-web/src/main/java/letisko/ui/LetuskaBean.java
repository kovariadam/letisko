package letisko.ui;

import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJBTransactionRolledbackException;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import letisko.api.CommonTO;
import letisko.api.LetuskaTO;
import letisko.exception.EntityNotAvailableException;
import letisko.service.LetiskoGenericService;

@Named("letuska")
@RequestScoped
public class LetuskaBean {

    @Inject
    private LetiskoGenericService letisko;
    private LetuskaTO letuskaTO;

    @PostConstruct
    public void init() {
        letuskaTO = new LetuskaTO();
    }

    public LetiskoGenericService getLetisko() {
        return letisko;
    }

    public void setLetisko(LetiskoGenericService letisko) {
        this.letisko = letisko;
    }

    public LetuskaTO getLetuskaTO() {
        return letuskaTO;
    }

    public void setLetuskaTO(LetuskaTO letuskaTO) {
        this.letuskaTO = letuskaTO;
    }

    public List<LetuskaTO> getVsetkyLetusky() {
        return letisko.vsetky(LetuskaTO.class);
    }

    public String uloz() {
        try {
            if (letisko.nacitaj(LetuskaTO.class, letuskaTO.getId()) == null) {
                letuskaTO.setId(null);
                letisko.vytvor(letuskaTO);
            } else {
                letisko.uprav(letuskaTO);
            }
        } catch (EntityNotAvailableException ex) {
            Logger.getLogger(LetuskaBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "letuska_list";
    }

    public void zmaz(Class<? extends CommonTO> type, Long id) {
        try {
            letisko.zmaz(type, id);
        } catch (EJBTransactionRolledbackException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            ResourceBundle bundle = ResourceBundle.getBundle(context.getApplication().getMessageBundle(), context.getViewRoot().getLocale());
            context.addMessage("zmaz", new FacesMessage(bundle.getString("EJBTransactionRolledbackException")));
        }
    }
}
