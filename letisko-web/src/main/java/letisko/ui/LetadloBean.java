package letisko.ui;

import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJBTransactionRolledbackException;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import letisko.api.CommonTO;
import letisko.api.LetadloTO;
import letisko.exception.EntityNotAvailableException;
import letisko.service.LetiskoGenericService;

/**
 * @author Martin Pitonak <mpitonak@mail.muni.cz>
 */
@Named("letadlo")
@RequestScoped
public class LetadloBean {

    @Inject
    private LetiskoGenericService letisko;
    private LetadloTO letadloTO;

    @PostConstruct
    public void init() {
        letadloTO = new LetadloTO();
    }

    public LetiskoGenericService getLetisko() {
        return letisko;
    }

    public void setLetisko(LetiskoGenericService letisko) {
        this.letisko = letisko;
    }

    public LetadloTO getLetadloTO() {
        return letadloTO;
    }

    public void setLetadloTO(LetadloTO letadloTO) {
        this.letadloTO = letadloTO;
    }

    public List<LetadloTO> getVsetkyLetadla() {
        return letisko.vsetky(LetadloTO.class);
    }

    public String uloz() {
        try {
            if (letisko.nacitaj(LetadloTO.class, letadloTO.getId()) == null) {
                letadloTO.setId(null);
                letisko.vytvor(letadloTO);
            } else {
                letisko.uprav(letadloTO);
            }
        } catch (EntityNotAvailableException ex) {
            Logger.getLogger(LetadloBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "letadlo_list";
    }

    public void zmaz(Class<? extends CommonTO> type, Long id) {
        try {
            letisko.zmaz(type, id);
        } catch (EJBTransactionRolledbackException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            ResourceBundle bundle = ResourceBundle.getBundle(context.getApplication().getMessageBundle(), context.getViewRoot().getLocale());
            context.addMessage("zmaz", new FacesMessage(bundle.getString("EJBTransactionRolledbackException")));
        }
    }
}
