package letisko.ui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.ejb.EJBTransactionRolledbackException;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import letisko.api.CommonTO;
import letisko.api.DestinaceTO;
import letisko.api.LetTO;
import letisko.api.LetadloTO;
import letisko.api.LetuskaTO;
import letisko.exception.EntityNotAvailableException;
import letisko.service.LetiskoGenericService;

/**
 *
 * @author Martin Pitonak <mpitonak@mail.muni.cz>
 */
@Named("let")
@RequestScoped
public class LetBean {
    @Inject
    private LetiskoGenericService letisko;
    private LetTO letTO;

    private Long selectedOdkud;
    private Long selectedKam;
    private Long selectedLetadlo;
    private String selectedOdlet;
    private String selectedPrilet;
    private List<Long> selectedLetusky;

    @PostConstruct
    public void init() {
        letTO = new LetTO();
    }

    public LetiskoGenericService getLetisko() {
        return letisko;
    }

    public void setLetisko(LetiskoGenericService letisko) {
        this.letisko = letisko;
    }

    public LetTO getLetTO() {
        return letTO;
    }

    public void setLetTO(LetTO letTO) {
        this.letTO = letTO;
        selectedKam = letTO.getDestinaceCil().getId();
        selectedOdkud = letTO.getDestinaceStart().getId();
        selectedLetadlo = letTO.getLetadlo().getId();
        selectedLetusky = new ArrayList<>(letTO.getLetusky().size());
        for (LetuskaTO letuska : letTO.getLetusky()) {
            selectedLetusky.add(letuska.getId());
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        selectedOdlet = sdf.format(letTO.getOdlet());
        selectedPrilet = sdf.format(letTO.getPrilet());
    }

    public Long getSelectedOdkud() {
        return selectedOdkud;
    }

    public void setSelectedOdkud(Long selectedOdkud) {
        this.selectedOdkud = selectedOdkud;
    }

    public Long getSelectedKam() {
        return selectedKam;
    }

    public void setSelectedKam(Long selectedKam) {
        this.selectedKam = selectedKam;
    }

    public String getSelectedOdlet() {
        return selectedOdlet;
    }

    public void setSelectedOdlet(String selectedOdlet) {
        this.selectedOdlet = selectedOdlet;
    }

    public String getSelectedPrilet() {
        return selectedPrilet;
    }

    public void setSelectedPrilet(String selectedPrilet) {
        this.selectedPrilet = selectedPrilet;
    }

    public List<Long> getSelectedLetusky() {
        return selectedLetusky;
    }

    public void setSelectedLetusky(List<Long> selectedLetusky) {
        this.selectedLetusky = selectedLetusky;
    }

    public Long getSelectedLetadlo() {
        return selectedLetadlo;
    }

    public void setSelectedLetadlo(Long selectedLetadlo) {
        this.selectedLetadlo = selectedLetadlo;
    }

    public List<LetTO> getVsetkyLety() {
        return letisko.vsetky(LetTO.class);
    }

    public List<LetuskaTO> getVsetkyLetusky() {
        return letisko.vsetky(LetuskaTO.class);
    }

    public List<LetadloTO> getVsetkyLetadla() {
        return letisko.vsetky(LetadloTO.class);
    }

    public List<DestinaceTO> getVsetkyDestinace() {
        return letisko.vsetky(DestinaceTO.class);
    }

    public String uloz() {
        try {
            if (selectedOdkud == selectedKam) {
                throw new ParseException("diffdest", 0);
            }
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date tempOdlet = sdf.parse(selectedOdlet);
            Date tempPrilet = sdf.parse(selectedPrilet);
            if (tempOdlet.after(tempPrilet)){
                throw new ParseException("reversedDates", 0);
            }
            letTO.setOdlet(tempOdlet);
            letTO.setPrilet(tempPrilet);
            List<LetuskaTO> letusky = new ArrayList<>();
            for (Long letuska : selectedLetusky) {
                letusky.add((LetuskaTO) letisko.nacitaj(LetuskaTO.class, letuska));
            }
            letTO.setLetusky(letusky);
            letTO.setDestinaceCil((DestinaceTO) letisko.nacitaj(DestinaceTO.class, selectedKam));
            letTO.setDestinaceStart((DestinaceTO) letisko.nacitaj(DestinaceTO.class, selectedOdkud));
            letTO.setLetadlo((LetadloTO) letisko.nacitaj(LetadloTO.class, selectedLetadlo));

            if (letisko.nacitaj(LetTO.class, letTO.getId()) == null) {
                letTO.setId(null);
                letisko.vytvor(letTO);
            } else {
                letisko.uprav(letTO);
            }
        } catch (ParseException | EntityNotAvailableException ex) {
            FacesContext context = FacesContext.getCurrentInstance();
            ResourceBundle bundle = ResourceBundle.getBundle(context.getApplication().getMessageBundle(), context.getViewRoot().getLocale());
            
            if (ex instanceof EntityNotAvailableException) context.addMessage("uloz", new FacesMessage(bundle.getString("EntityNotAvailableException")));
            else if (ex.getMessage().equals("diffdest")) context.addMessage("uloz", new FacesMessage(bundle.getString("NotDifferentDestinations")));
            else if (ex.getMessage().equals("reversedDates")) context.addMessage("uloz", new FacesMessage(bundle.getString("reversedDates")));
            
            return "let_edit";
        }
        return "let_list";
    }

    public void zmaz(Class<? extends CommonTO> type, Long id) {
        try{
            letisko.zmaz(type, id);
        }catch(EJBTransactionRolledbackException e){
            FacesContext context = FacesContext.getCurrentInstance();
            ResourceBundle bundle = ResourceBundle.getBundle(context.getApplication().getMessageBundle(), context.getViewRoot().getLocale());
            context.addMessage("zmaz", new FacesMessage(bundle.getString("EJBTransactionRolledbackException")));
        }
    }

}
