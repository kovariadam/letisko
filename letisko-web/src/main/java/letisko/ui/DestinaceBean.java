package letisko.ui;

import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJBTransactionRolledbackException;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import letisko.api.CommonTO;
import letisko.api.DestinaceTO;
import letisko.exception.EntityNotAvailableException;
import letisko.service.LetiskoGenericService;

/**
 * User: akovari Date: 10/19/13 Time: 7:32 PM
 */
@Named("destinace")
@RequestScoped
public class DestinaceBean {

    @Inject
    private LetiskoGenericService letisko;
    private DestinaceTO destinaceTO;

    @PostConstruct
    public void init() {
        destinaceTO = new DestinaceTO();
    }

    public LetiskoGenericService getLetisko() {
        return letisko;
    }

    public void setLetisko(LetiskoGenericService letisko) {
        this.letisko = letisko;
    }

    public DestinaceTO getDestinaceTO() {
        return destinaceTO;
    }

    public void setDestinaceTO(DestinaceTO destinaceTO) {
        this.destinaceTO = destinaceTO;
    }

    public List<DestinaceTO> getVsetkyDestinace() {
        return letisko.vsetky(DestinaceTO.class);
    }

    public String uloz() {
        try {
            if (letisko.nacitaj(DestinaceTO.class, destinaceTO.getId()) == null) {
                destinaceTO.setId(null);
                letisko.vytvor(destinaceTO);
            } else {
                letisko.uprav(destinaceTO);
            }
        } catch (EntityNotAvailableException ex) {
            Logger.getLogger(DestinaceBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "destinace_list";
    }

    public void zmaz(Class<? extends CommonTO> type, Long id) {
        try {
            letisko.zmaz(type, id);
        } catch (EJBTransactionRolledbackException e) {
            FacesContext context = FacesContext.getCurrentInstance();
            ResourceBundle bundle = ResourceBundle.getBundle(context.getApplication().getMessageBundle(), context.getViewRoot().getLocale());
            context.addMessage("zmaz", new FacesMessage(bundle.getString("EJBTransactionRolledbackException")));
        }
    }
}
