package letisko.rest;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import letisko.api.CommonTO;
import letisko.exception.EntityNotAvailableException;
import letisko.service.LetiskoGenericService;

public abstract class CommonRESTFacade<T extends CommonTO> {
    @Inject
    protected LetiskoGenericService letisko;

    protected abstract Class<T> supportedClass();

    public Response vytvor(T obj) {
        try {
            if (obj.getId() == null) {
                letisko.vytvor(obj);
                return Response.ok("Created").status(201).build();
            } else {
                letisko.uprav(obj);
                return Response.ok("Modified.").build();
            }
        } catch (EntityNotAvailableException e) {
            return Response.ok("Bad request.").status(400).build();
        }
    }

    public Set<T> vsetky() {
        List<T> letusky = letisko.vsetky(supportedClass());
        if (letusky == null) {
            return new HashSet<>();
        }
        return new HashSet<>(letusky);
    }
    
    public Response zmaz(@PathParam("id") Long id) {
        letisko.zmaz(supportedClass(), id);
        return Response.noContent().build();
    }
}
