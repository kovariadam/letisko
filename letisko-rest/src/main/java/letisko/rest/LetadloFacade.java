package letisko.rest;

import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import letisko.api.LetadloTO;

@Path("letadlo")
public class LetadloFacade extends CommonRESTFacade<LetadloTO> {
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    @Override
    public Response vytvor(LetadloTO obj) {
        return super.vytvor(obj);
    }

    @GET
    @Produces(MediaType.APPLICATION_XML)
    @Override
    public Set<LetadloTO> vsetky() {
        return super.vsetky();
    }

    @Override
    protected Class<LetadloTO> supportedClass() {
        return LetadloTO.class;
    }
    
    @DELETE
    @Path("{id}")
    @Override
    public Response zmaz(@PathParam("id") Long id) {
        return super.zmaz(id);
    }
}
