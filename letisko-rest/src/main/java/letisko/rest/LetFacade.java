package letisko.rest;

import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import letisko.api.LetTO;

@Path("let")
public class LetFacade extends CommonRESTFacade<LetTO> {
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    @Override
    public Response vytvor(LetTO obj) {
        return super.vytvor(obj);
    }

    @GET
    @Produces(MediaType.APPLICATION_XML)
    @Override
    public Set<LetTO> vsetky() {
        return super.vsetky();
    }

    @Override
    protected Class<LetTO> supportedClass() {
        return LetTO.class;
    }
 
    @DELETE
    @Path("{id}")
    @Override
    public Response zmaz(@PathParam("id") Long id) {
        return super.zmaz(id);
    }
}
