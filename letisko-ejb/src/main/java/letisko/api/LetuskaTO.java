package letisko.api;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Objekt poskytujuci nezavisle aplikacne rozhranie reprezentujuce objekt "Letuska"
 */
@XmlRootElement
public class LetuskaTO extends CommonTO {
    private String meno;
    private String priezvisko;

    public LetuskaTO() {
    }

    public LetuskaTO(String meno, String priezvisko) {
        this.meno = meno;
        this.priezvisko = priezvisko;
    }

    public String getMeno() {

        return meno;
    }

    public void setMeno(String meno) {
        this.meno = meno;
    }

    public String getPriezvisko() {
        return priezvisko;
    }

    public void setPriezvisko(String priezvisko) {
        this.priezvisko = priezvisko;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LetuskaTO letuskaTO = (LetuskaTO) o;

        if (meno != null ? !meno.equals(letuskaTO.meno) : letuskaTO.meno != null) return false;
        if (priezvisko != null ? !priezvisko.equals(letuskaTO.priezvisko) : letuskaTO.priezvisko != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = meno != null ? meno.hashCode() : 0;
        result = 31 * result + (priezvisko != null ? priezvisko.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LetuskaTO{" +
                "meno='" + meno + '\'' +
                ", priezvisko='" + priezvisko + '\'' +
                '}';
    }
}
