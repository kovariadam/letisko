package letisko.api;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Objekt poskytujuci nezavisle aplikacne rozhranie reprezentujuce objekt "Letadlo"
 */
@XmlRootElement
public class LetadloTO extends CommonTO {
    private Integer kapacita;
    private String nazov;
    private String typ;

    public LetadloTO() {
    }

    public LetadloTO(Integer kapacita, String nazov, String typ) {
        this.kapacita = kapacita;
        this.nazov = nazov;
        this.typ = typ;
    }

    public Integer getKapacita() {
        return kapacita;
    }

    public void setKapacita(Integer kapacita) {
        this.kapacita = kapacita;
    }

    public String getNazov() {
        return nazov;
    }

    public void setNazov(String nazov) {
        this.nazov = nazov;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        LetadloTO letadloTO = (LetadloTO) o;

        if (kapacita != null ? !kapacita.equals(letadloTO.kapacita) : letadloTO.kapacita != null) return false;
        if (nazov != null ? !nazov.equals(letadloTO.nazov) : letadloTO.nazov != null) return false;
        if (typ != null ? !typ.equals(letadloTO.typ) : letadloTO.typ != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (kapacita != null ? kapacita.hashCode() : 0);
        result = 31 * result + (nazov != null ? nazov.hashCode() : 0);
        result = 31 * result + (typ != null ? typ.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LetadloTO{" +
                "kapacita=" + kapacita +
                ", nazov='" + nazov + '\'' +
                ", typ='" + typ + '\'' +
                '}';
    }
}
