package letisko.api.transformations;

import javax.inject.Named;

import letisko.api.LetuskaTO;
import letisko.model.Letuska;

/**
 * Konvertor typov Letuska z entity na rozhranie a naopak
 */
@Named
public class LetuskaConverter implements TypeConverter<Letuska, LetuskaTO> {
    @Override
    public LetuskaTO fromEntity(Letuska letuska) {
        if (letuska == null) {
            return null;
        }
        LetuskaTO letuskaTO = new LetuskaTO(letuska.getMeno(), letuska.getPriezvisko());
        letuskaTO.setId(letuska.getId());

        return letuskaTO;
    }

    @Override
    public Letuska toEntity(LetuskaTO letuskaTO) {
        if (letuskaTO == null) {
            return null;
        }
        Letuska letuska = new Letuska(letuskaTO.getMeno(), letuskaTO.getPriezvisko());
        letuska.setId(letuskaTO.getId());
        return letuska;
    }
}
