package letisko.api.transformations;

import javax.inject.Named;

import letisko.api.DestinaceTO;
import letisko.model.Destinace;

/**
 * Konvertor typov Destinace z entity na rozhranie a naopak
 */
@Named
public class DestinaceConverter implements TypeConverter<Destinace, DestinaceTO> {

    @Override
    public DestinaceTO fromEntity(Destinace destinace) {
        if (destinace == null) {
            return null;
        }
        DestinaceTO destinaceTO = new DestinaceTO(destinace.getStat(), destinace.getMesto());
        destinaceTO.setId(destinace.getId());
        return destinaceTO;
    }

    @Override
    public Destinace toEntity(DestinaceTO destinaceTO) {
        if (destinaceTO == null) {
            return null;
        }
        Destinace destinace = new Destinace(destinaceTO.getStat(), destinaceTO.getMesto());
        destinace.setId(destinaceTO.getId());
        return destinace;
    }
}
