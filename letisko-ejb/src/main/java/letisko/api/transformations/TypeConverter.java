package letisko.api.transformations;

import letisko.api.CommonTO;
import letisko.model.CommonEntity;

/**
 * Rozhranie definujuce operaciu prevodu entity na transfer-object a spat
 *
 * @param <F> FROM
 * @param <T> TO
 */
public interface TypeConverter<F extends CommonEntity, T extends CommonTO> {
    public T fromEntity(F entity);

    public F toEntity(T to);
}
