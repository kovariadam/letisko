package letisko.api.transformations;

import javax.inject.Named;

import letisko.api.LetadloTO;
import letisko.model.Letadlo;

/**
 * Konvertor typov Letadlo z entity na rozhranie a naopak
 */
@Named
public class LetadloConverter implements TypeConverter<Letadlo, LetadloTO> {
    @Override
    public LetadloTO fromEntity(Letadlo letadlo) {
        if (letadlo == null) {
            return null;
        }
        LetadloTO LetadloTO = new LetadloTO(letadlo.getKapacita(), letadlo.getNazov(), letadlo.getTyp());
        LetadloTO.setId(letadlo.getId());

        return LetadloTO;
    }

    @Override
    public Letadlo toEntity(LetadloTO letadloTO) {
        if (letadloTO == null) {
            return null;
        }
        Letadlo letadlo = new Letadlo(letadloTO.getKapacita(), letadloTO.getNazov(), letadloTO.getTyp());
        letadlo.setId(letadloTO.getId());
        return letadlo;
    }
}
