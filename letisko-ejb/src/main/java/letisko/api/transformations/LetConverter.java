package letisko.api.transformations;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import letisko.api.DestinaceTO;
import letisko.api.LetTO;
import letisko.api.LetadloTO;
import letisko.api.LetuskaTO;
import letisko.model.Destinace;
import letisko.model.Let;
import letisko.model.Letadlo;
import letisko.model.Letuska;

/**
 * Konvertor typov Let z entity na rozhranie a naopak
 */
@Named
public class LetConverter implements TypeConverter<Let, LetTO> {
    @Inject
    private TypeConverter<Letadlo, LetadloTO> letadloConverter;
    @Inject
    private TypeConverter<Destinace, DestinaceTO> destinaceConverter;
    @Inject
    private TypeConverter<Letuska, LetuskaTO> letuskaConverter;

    @Override
    public LetTO fromEntity(Let let) {
        if (let == null) {
            return null;
        }
        LetTO letTO = new LetTO(letadloConverter.fromEntity(let.getLetadlo()),
                destinaceConverter.fromEntity(let.getDestinaceStart()),
                destinaceConverter.fromEntity(let.getDestinaceCil()),
                let.getOdlet(),
                let.getPrilet(),
                convertLetusky(let));
        letTO.setId(let.getId());
        return letTO;
    }

    @Override
    public Let toEntity(LetTO letTO) {
        if (letTO == null) {
            return null;
        }
        Let let = new Let(letadloConverter.toEntity(letTO.getLetadlo()),
                destinaceConverter.toEntity(letTO.getDestinaceStart()),
                destinaceConverter.toEntity(letTO.getDestinaceCil()),
                letTO.getOdlet(),
                letTO.getPrilet(),
                convertLetuskyTOs(letTO));
        let.setId(letTO.getId());
        return let;
    }

    private List<LetuskaTO> convertLetusky(Let let) {
        List<LetuskaTO> letuskaTOs;

        if (let.getLetusky() != null) {
            letuskaTOs = new ArrayList<>(let.getLetusky().size());
            for (Letuska letuska : let.getLetusky()) {
                letuskaTOs.add(letuskaConverter.fromEntity(letuska));
            }
        } else {
            letuskaTOs = null;
        }
        return letuskaTOs;
    }

    private List<Letuska> convertLetuskyTOs(LetTO let) {
        List<Letuska> letuskas;

        if (let.getLetusky() != null) {
            letuskas = new ArrayList<>(let.getLetusky().size());
            for (LetuskaTO letuska : let.getLetusky()) {
                letuskas.add(letuskaConverter.toEntity(letuska));
            }
        } else {
            letuskas = null;
        }
        return letuskas;
    }

}
