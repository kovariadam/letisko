package letisko.api;

import java.io.Serializable;

/**
 * Spolocna trieda pre transfer objekty
 * @param <ID> typ ID objektu
 */
public abstract class CommonTO implements Serializable {
    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "CommonTO{" +
                "id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommonTO commonTO = (CommonTO) o;

        if (id != null ? !id.equals(commonTO.id) : commonTO.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
