package letisko.api;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Objekt poskytujuci nezavisle aplikacne rozhranie reprezentujuce objekt "Let"
 */
@XmlRootElement
public class LetTO extends CommonTO {

    private LetadloTO letadlo;
    private DestinaceTO destinaceStart;
    private DestinaceTO destinaceCil;
    private Date odlet;
    private Date prilet;
    private List<LetuskaTO> letusky;

    public LetTO() {
    }

    public LetTO(LetadloTO letadlo, DestinaceTO destinaceStart, DestinaceTO destinaceCil, Date odlet, Date prilet, List<LetuskaTO> letusky) {
        this.letadlo = letadlo;
        this.destinaceStart = destinaceStart;
        this.destinaceCil = destinaceCil;
        this.odlet = odlet;
        this.prilet = prilet;
        this.letusky = letusky;
    }

    public LetadloTO getLetadlo() {
        return letadlo;
    }

    public void setLetadlo(LetadloTO letadlo) {
        this.letadlo = letadlo;
    }

    public DestinaceTO getDestinaceStart() {
        return destinaceStart;
    }

    public void setDestinaceStart(DestinaceTO destinaceStart) {
        this.destinaceStart = destinaceStart;
    }

    public DestinaceTO getDestinaceCil() {
        return destinaceCil;
    }

    public void setDestinaceCil(DestinaceTO destinaceCil) {
        this.destinaceCil = destinaceCil;
    }

    public Date getOdlet() {
        return odlet;
    }

    public void setOdlet(Date odlet) {
        this.odlet = odlet;
    }

    public Date getPrilet() {
        return prilet;
    }

    public void setPrilet(Date prilet) {
        this.prilet = prilet;
    }

    public List<LetuskaTO> getLetusky() {
        return letusky;
    }

    public void setLetusky(List<LetuskaTO> letusky) {
        this.letusky = letusky;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        LetTO letTO = (LetTO) o;

        if (destinaceCil != null ? !destinaceCil.equals(letTO.destinaceCil) : letTO.destinaceCil != null) return false;
        if (destinaceStart != null ? !destinaceStart.equals(letTO.destinaceStart) : letTO.destinaceStart != null)
            return false;
        if (letadlo != null ? !letadlo.equals(letTO.letadlo) : letTO.letadlo != null) return false;
        if (letusky != null ? !letusky.equals(letTO.letusky) : letTO.letusky != null) return false;
        if (odlet != null ? !odlet.equals(letTO.odlet) : letTO.odlet != null) return false;
        if (prilet != null ? !prilet.equals(letTO.prilet) : letTO.prilet != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (letadlo != null ? letadlo.hashCode() : 0);
        result = 31 * result + (destinaceStart != null ? destinaceStart.hashCode() : 0);
        result = 31 * result + (destinaceCil != null ? destinaceCil.hashCode() : 0);
        result = 31 * result + (odlet != null ? odlet.hashCode() : 0);
        result = 31 * result + (prilet != null ? prilet.hashCode() : 0);
        result = 31 * result + (letusky != null ? letusky.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LetTO{" + "letadlo=" + letadlo + ", destinaceStart=" + destinaceStart + ", destinaceCil=" + destinaceCil + ", odlet=" + odlet + ", prilet=" + prilet + ", letusky=" + letusky + '}';
    }
}
