package letisko.beans;

import letisko.api.CommonTO;
import letisko.api.LetadloTO;
import letisko.api.transformations.TypeConverter;
import letisko.model.Letadlo;

import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * DAO trieda implementujuca CRUD operacie nad entitou Letadlo
 */
@Stateless
public class LetadloDAO extends CRUDBean<Letadlo, LetadloTO> {
    @Inject
    private TypeConverter<Letadlo, LetadloTO> letadloTypeConverter;

    @Override
    protected TypeConverter<Letadlo, LetadloTO> getTypeConverter() {
        return letadloTypeConverter;
    }

    @Override
    protected Class<?> getEntityClass() {
        return Letadlo.class;
    }

    @Override
    public Class<? extends CommonTO> getTOClass() {
        return LetadloTO.class;
    }
}
