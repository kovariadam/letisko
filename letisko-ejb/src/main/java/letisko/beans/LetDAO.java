package letisko.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.TemporalType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import letisko.api.CommonTO;
import letisko.api.LetTO;
import letisko.api.transformations.TypeConverter;
import letisko.model.Destinace;
import letisko.model.Let;
import letisko.model.Letadlo;
import letisko.model.Letuska;

/**
 * DAO trieda implementujuca CRUD operacie nad entitou Let
 */
@Stateless
public class LetDAO extends CRUDBean<Let, LetTO> {
    @Inject
    private TypeConverter<Let, LetTO> letTypeConverter;

    @Override
    protected TypeConverter<Let, LetTO> getTypeConverter() {
        return letTypeConverter;
    }

    @Override
    protected Class<?> getEntityClass() {
        return Let.class;
    }

    @Override
    public Class<? extends CommonTO> getTOClass() {
        return LetTO.class;
    }

    /**
     * Vytvori entitu Letu v DB nastavene parametre(subentity) Letu musia byt uz
     * v DB
     * 
     * @param obj
     */
    private void vytvor_internal(CommonTO obj) {
        Let let = getTypeConverter().toEntity((LetTO) obj);
        let.setLetadlo(em.find(Letadlo.class, let.getLetadlo().getId()));
        let.setDestinaceCil(em.find(Destinace.class, let.getDestinaceCil().getId()));
        let.setDestinaceStart(em.find(Destinace.class, let.getDestinaceStart().getId()));
        List<Letuska> novy = null;
        if (let.getLetusky() != null) {
            novy = new ArrayList<>(let.getLetusky().size());
            for (Letuska letuska : let.getLetusky()) {
                novy.add(em.find(Letuska.class, letuska.getId()));
            }
        }
        let.setLetusky(novy);
        em.persist(let);
        obj.setId(let.getId());
    }

    @Override
    public void vytvor(CommonTO obj) {
        vytvor_internal(obj);
    }

    /**
     * Upravi entitu Let v DB nastavene parametre(subentity) Letu musia byt uz v
     * DB
     * 
     * @param obj
     */
    private void uprav_internal(CommonTO obj) {
        Let oldEntity = getTypeConverter().toEntity((LetTO) obj);
        Let newEntity = em.find(Let.class, obj.getId());
        newEntity.setDestinaceCil(em.find(Destinace.class, oldEntity.getDestinaceCil().getId()));
        newEntity.setDestinaceStart(em.find(Destinace.class, oldEntity.getDestinaceStart().getId()));
        newEntity.setLetadlo(em.find(Letadlo.class, oldEntity.getLetadlo().getId()));
        List<Letuska> novy = null;
        if (oldEntity.getLetusky() != null) {
            novy = new ArrayList<>(oldEntity.getLetusky().size());
            for (Letuska letuska : oldEntity.getLetusky()) {
                novy.add(em.find(Letuska.class, letuska.getId()));
            }
        }
        newEntity.setOdlet(oldEntity.getOdlet());
        newEntity.setPrilet(oldEntity.getPrilet());
        newEntity.setLetusky(novy);
        newEntity.setId(oldEntity.getId());
    }

    @Override
    public void uprav(CommonTO obj) {
        uprav_internal(obj);
    }

    /**
     * Kontrola ci letusky a letadlo noveho/upraveneho letu sa nachadzaju v jeho
     * case aj v inych letoch. Ak id entity == null, entita je nova, teda sa
     * nekontroluje. Predpoklada sa, ze je zadane Letadlo a casy odletu/priletu.
     * Letusky nepovinne.
     */
    public boolean isAvailable(LetTO letTo) {
        Let let = letTypeConverter.toEntity(letTo);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Object> cq = cb.createQuery();
        Root<Let> letDb = cq.from(Let.class);

        List<Predicate> predORs = new ArrayList<>();
        if (let.getLetadlo().getId() != null)
            predORs.add(cb.equal(cb.parameter(Letadlo.class, "letLetadlo"), letDb.<Letadlo> get("letadlo")));
        if (let.getLetusky() != null && !let.getLetusky().isEmpty())
            for (Letuska letuska : let.getLetusky())
                if (letuska.getId() != null)
                    predORs.add(cb.isMember(letuska, letDb.<List<Letuska>> get("letusky")));

        Predicate predAND = cb.and(cb.lessThan(cb.parameter(Date.class, "letOdlet"), letDb.<Date> get("prilet")),
                                   cb.lessThan(letDb.<Date> get("odlet"), cb.parameter(Date.class, "letPrilet")));
        if (let.getId() != null) {
            predAND = cb.and(predAND, cb.notEqual(letDb.get("id"), let.getId()));
        } else if (predORs.isEmpty())
            return true;

        return ((Number) em.createQuery(cq.select(cb.count(letDb)).where(cb.and(predAND, cb.or(predORs.toArray(new Predicate[0])))))
                .setParameter("letPrilet", let.getPrilet(), TemporalType.TIMESTAMP)
                .setParameter("letOdlet", let.getOdlet(), TemporalType.TIMESTAMP)
                .setParameter("letLetadlo", let.getLetadlo()).getSingleResult()).intValue() <= 0;
    }
}
