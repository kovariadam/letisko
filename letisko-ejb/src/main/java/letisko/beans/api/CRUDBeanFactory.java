package letisko.beans.api;

import letisko.api.CommonTO;
import letisko.beans.CRUDBean;
import letisko.model.CommonEntity;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * User: akovari
 * Date: 10/19/13
 * Time: 7:47 PM
 */
@Named
public class CRUDBeanFactory {
    @Inject
    @Any
    private Instance<CRUDBean<? extends CommonEntity, ? extends CommonTO>> crudBeans;

    public CRUDBean<? extends CommonEntity, ? extends CommonTO> getCRUDBean(Class<? extends CommonTO> toClass) {
        for (CRUDBean<? extends CommonEntity, ? extends CommonTO> crudBean : crudBeans) {
            if (toClass.equals(crudBean.getTOClass())) {
                return crudBean;
            }
        }

        return null;
    }
}
