package letisko.beans;

import letisko.api.CommonTO;
import letisko.api.transformations.TypeConverter;
import letisko.model.CommonEntity;
import org.apache.commons.beanutils.BeanUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import javax.validation.ConstraintViolationException;

/**
 * Create, Read, Update and Delete operations DAO abstract class
 * User: akovari
 * Date: 10/11/13
 * Time: 5:53 PM
 */
public abstract class CRUDBean<F extends CommonEntity, T extends CommonTO> {
    @PersistenceContext
    protected EntityManager em;

    protected abstract TypeConverter<F, T> getTypeConverter();

    protected abstract Class<?> getEntityClass();

    public abstract Class<? extends CommonTO> getTOClass();

    private void vytvor_internal(T obj) {
        CommonEntity entity = getTypeConverter().toEntity(obj);
        em.persist(entity);
        obj.setId(entity.getId());
    }

    private void zmaz_internal(Long id) {
        em.remove(em.find(getEntityClass(), id));
    }

    private T nacitaj_internal(Long id) {
        return getTypeConverter().fromEntity((F) em.find(getEntityClass(), id));
    }

    private void uprav_internal(T obj) {
        F oldEntity = getTypeConverter().toEntity(obj);
        F newEntity = (F) em.find(getEntityClass(), obj.getId());
        try {
            BeanUtils.copyProperties(newEntity, oldEntity);
            newEntity.setId((Long) obj.getId());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private List<T> vsetky_internal() {
        List<F> listEntity = (List<F>) em.createQuery("select x from " + getEntityClass().getSimpleName() + " X", getEntityClass()).getResultList();
        List<T> listTO = new ArrayList<T>(listEntity.size());
        for (F obj : listEntity) {
            listTO.add(getTypeConverter().fromEntity(obj));
        }
        return listTO;
    }

    public void vytvor(CommonTO obj) {
        vytvor_internal((T) obj);
    }

    public void zmaz(Long id) {
        zmaz_internal(id);
    }

    public void uprav(CommonTO obj) {
        uprav_internal((T) obj);
    }

    public <T extends CommonTO> List<T> vsetky() {
        return (List<T>) vsetky_internal();
    }

    public <T extends CommonTO> T nacitaj(Long id) {
        return (T) nacitaj_internal(id);
    }
}
