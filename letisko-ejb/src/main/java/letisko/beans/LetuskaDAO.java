package letisko.beans;

import letisko.api.CommonTO;
import letisko.api.LetuskaTO;
import letisko.api.transformations.TypeConverter;
import letisko.model.Letuska;

import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * DAO trieda implementujuca CRUD operacie nad entitou Letuska
 */
@Stateless
public class LetuskaDAO extends CRUDBean<Letuska, LetuskaTO> {
    @Inject
    private TypeConverter<Letuska, LetuskaTO> letuskaTypeConverter;

    @Override
    protected TypeConverter<Letuska, LetuskaTO> getTypeConverter() {
        return letuskaTypeConverter;
    }

    @Override
    protected Class<?> getEntityClass() {
        return Letuska.class;
    }

    @Override
    public Class<? extends CommonTO> getTOClass() {
        return LetuskaTO.class;
    }
}
