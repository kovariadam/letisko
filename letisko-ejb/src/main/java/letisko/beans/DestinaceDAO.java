package letisko.beans;

import javax.ejb.Stateless;
import javax.inject.Inject;

import letisko.api.CommonTO;
import letisko.api.DestinaceTO;
import letisko.api.transformations.TypeConverter;
import letisko.model.Destinace;

/**
 * DAO trieda implementujuca CRUD operacie nad entitou Destinace
 */
@Stateless
public class DestinaceDAO extends CRUDBean<Destinace, DestinaceTO> {
    @Inject
    private TypeConverter<Destinace, DestinaceTO> destinaceTypeConverter;

    @Override
    protected TypeConverter<Destinace, DestinaceTO> getTypeConverter() {
        return destinaceTypeConverter;
    }

    @Override
    protected Class<?> getEntityClass() {
        return Destinace.class;
    }

    @Override
    public Class<? extends CommonTO> getTOClass() {
        return DestinaceTO.class;
    }
}
