package letisko.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Entitna trieda reprezentujuca Let
 */
@Entity
public class Let extends CommonEntity {
    @ManyToOne(cascade = {CascadeType.PERSIST})
    private Letadlo letadlo;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    private Destinace destinaceStart;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    private Destinace destinaceCil;

    @Column
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date odlet;

    @Column
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date prilet;

    @ManyToMany(cascade = {CascadeType.PERSIST})
    private List<Letuska> letusky;

    public Let() {
    }

    public Let(Letadlo letadlo, Destinace destinaceStart, Destinace destinaceCil, Date odlet, Date prilet, List<Letuska> letusky) {
        this.letadlo = letadlo;
        this.destinaceStart = destinaceStart;
        this.destinaceCil = destinaceCil;
        this.odlet = odlet;
        this.prilet = prilet;
        this.letusky = letusky;
    }

    public void setLetadlo(Letadlo letadlo) {
        this.letadlo = letadlo;
    }

    public Letadlo getLetadlo() {
        return this.letadlo;
    }

    public void setDestinaceStart(Destinace destinaceStart) {
        this.destinaceStart = destinaceStart;
    }

    public Destinace getDestinaceStart() {
        return destinaceStart;
    }

    public void setDestinaceCil(Destinace destinaceCil) {
        this.destinaceCil = destinaceCil;
    }

    public Destinace getDestinaceCil() {
        return destinaceCil;
    }

    public void setOdlet(Date odlet) {
        this.odlet = odlet;
    }

    public Date getOdlet() {
        return odlet;
    }

    public void setPrilet(Date prilet) {
        this.prilet = prilet;
    }

    public Date getPrilet() {
        return prilet;
    }

    public List<Letuska> getLetusky() {
        return letusky;
    }

    public void setLetusky(List<Letuska> letusky) {
        this.letusky = letusky;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Let let = (Let) o;

        if (id != null ? !id.equals(let.id) : let.id != null) return false;
        if (destinaceStart != null ? !destinaceStart.equals(let.destinaceStart) : let.destinaceStart != null)
            return false;
        if (destinaceCil != null ? !destinaceCil.equals(let.destinaceCil) : let.destinaceCil != null) return false;
        if (odlet != null ? !odlet.equals(let.odlet) : let.odlet != null) return false;
        if (prilet != null ? !prilet.equals(let.prilet) : let.prilet != null) return false;

        return true;
    }


    @Override
    public int hashCode() {
        int hash = id != null ? id.hashCode() : 0;
        hash = 31 * hash + (letadlo != null ? letadlo.hashCode() : 0);
        hash = 31 * hash + (destinaceStart != null ? destinaceStart.hashCode() : 0);
        hash = 31 * hash + (destinaceCil != null ? destinaceCil.hashCode() : 0);
        hash = 31 * hash + (prilet != null ? prilet.hashCode() : 0);
        hash = 31 * hash + (odlet != null ? odlet.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "LetTO{id=" + id + ", letadlo=" + letadlo + ", destinaceStart=" + destinaceStart + ", destinaceCil=" + destinaceCil + ", odlet=" + odlet + ", prilet=" + prilet + ", letusky=" + letusky + '}';
    }

}
