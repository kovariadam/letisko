package letisko.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;

/**
 * Entitna trieda reprezentujuca Destinace
 */
@Entity
public class Destinace extends CommonEntity {
    @Column
    private String stat;

    @Column
    private String mesto;

    public Destinace() {
    }

    public Destinace(String stat, String mesto) {
        this.stat = stat;
        this.mesto = mesto;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public String getMesto() {
        return mesto;
    }

    public void setMesto(String mesto) {
        this.mesto = mesto;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        hash = 97 * hash + Objects.hashCode(this.stat);
        hash = 97 * hash + Objects.hashCode(this.mesto);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Destinace other = (Destinace) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.stat, other.stat)) {
            return false;
        }
        if (!Objects.equals(this.mesto, other.mesto)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Destinace{" + "id=" + id + ", stat='" + stat + "', mesto='" + mesto + "'}";
    }
}
