package letisko.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Entitna trieda reprezentujuca Letusku
 */
@Entity
public class Letuska extends CommonEntity {
    @Column
    private String meno;
    @Column
    private String priezvisko;

    public Letuska() {
    }

    public Letuska(String meno, String priezvisko) {

        this.meno = meno;
        this.priezvisko = priezvisko;
    }

    public String getMeno() {
        return meno;
    }

    public void setMeno(String meno) {
        this.meno = meno;
    }

    public String getPriezvisko() {
        return priezvisko;
    }

    public void setPriezvisko(String priezvisko) {
        this.priezvisko = priezvisko;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Letuska letuska = (Letuska) o;

        if (id != null ? !id.equals(letuska.id) : letuska.id != null) return false;
        if (meno != null ? !meno.equals(letuska.meno) : letuska.meno != null) return false;
        if (priezvisko != null ? !priezvisko.equals(letuska.priezvisko) : letuska.priezvisko != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (meno != null ? meno.hashCode() : 0);
        result = 31 * result + (priezvisko != null ? priezvisko.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Letuska{" +
                "id=" + id +
                ", meno='" + meno + '\'' +
                ", priezvisko='" + priezvisko + '\'' +
                '}';
    }
}
