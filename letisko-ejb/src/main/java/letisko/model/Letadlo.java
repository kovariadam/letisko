package letisko.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import java.io.Serializable;

/**
 * Entitna trieda reprezentujuca Letadlo
 */
@Entity
public class Letadlo extends CommonEntity {
    @Min(0)
    private Integer kapacita;
    private String nazov;
    private String typ;

    public Letadlo() {
    }

    public Letadlo(Integer kapacita, String nazov, String typ) {

        this.kapacita = kapacita;
        this.nazov = nazov;
        this.typ = typ;
    }

    public Integer getKapacita() {
        return kapacita;
    }

    public void setKapacita(Integer kapacita) {
        this.kapacita = kapacita;
    }

    public String getNazov() {
        return nazov;
    }

    public void setNazov(String nazov) {
        this.nazov = nazov;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Letadlo letadlo = (Letadlo) o;

        if (id != null ? !id.equals(letadlo.id) : letadlo.id != null) return false;
        if (kapacita != null ? !kapacita.equals(letadlo.kapacita) : letadlo.kapacita != null) return false;
        if (nazov != null ? !nazov.equals(letadlo.nazov) : letadlo.nazov != null) return false;
        if (typ != null ? !typ.equals(letadlo.typ) : letadlo.typ != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (kapacita != null ? kapacita.hashCode() : 0);
        result = 31 * result + (nazov != null ? nazov.hashCode() : 0);
        result = 31 * result + (typ != null ? typ.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Letadlo{" +
                "id=" + id +
                ", kapacita=" + kapacita +
                ", nazov='" + nazov + '\'' +
                ", typ='" + typ + '\'' +
                '}';
    }
}
