package letisko.service;

import letisko.api.CommonTO;
import letisko.beans.api.CRUDBeanFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import letisko.api.LetTO;
import letisko.beans.CRUDBean;
import letisko.beans.LetDAO;
import letisko.exception.EntityNotAvailableException;
import letisko.model.CommonEntity;
import org.jboss.ejb3.annotation.SecurityDomain;

/**
 * Letisko Service layer implementation
 * User: akovari
 * Date: 10/19/13
 * Time: 7:45 PM
 */
@Stateless
@SecurityDomain("letisko")
@RolesAllowed({"users","admins"})
public class LetiskoGenericService {
    @Inject
    CRUDBeanFactory crudBeanFactory;
    
    public void vytvor(CommonTO obj) throws EntityNotAvailableException{
        CRUDBean<? extends CommonEntity, ? extends CommonTO> bean = crudBeanFactory.getCRUDBean(obj.getClass());
        if (obj instanceof LetTO) {
            if ( !((LetDAO)bean).isAvailable((LetTO)obj)) throw new EntityNotAvailableException("Letusku/lietadlo nie je mozne priradit k tomuto letu.");
        }
        bean.vytvor(obj);
    }

    public void zmaz(Class<? extends CommonTO> type, Long id) {
        crudBeanFactory.getCRUDBean(type).zmaz(id);
    }

    public void uprav(CommonTO obj) throws EntityNotAvailableException {
        CRUDBean<? extends CommonEntity, ? extends CommonTO> bean = crudBeanFactory.getCRUDBean(obj.getClass());
        if (obj instanceof LetTO) {
            if ( !((LetDAO)bean).isAvailable((LetTO)obj)) throw new EntityNotAvailableException("Letusku/lietadlo nie je mozne priradit k tomuto letu.");
        }
        bean.uprav(obj);
    }

    @PermitAll
    public <T extends CommonTO> List<T> vsetky(Class<? extends CommonTO> type) {
        return crudBeanFactory.getCRUDBean(type).vsetky();
    }
    
    public <T extends CommonTO> T nacitaj(Class<? extends CommonTO> type, Long id) {
        return crudBeanFactory.getCRUDBean(type).nacitaj(id);
    }
}
