
package letisko.exception;

/**
 * 
 * @author Martin Pitonak <mpitonak@mail.muni.cz>
 */
public class EntityNotAvailableException extends Exception {

    public EntityNotAvailableException() {
    }

    public EntityNotAvailableException(String msg) {
        super(msg);
    }
}
