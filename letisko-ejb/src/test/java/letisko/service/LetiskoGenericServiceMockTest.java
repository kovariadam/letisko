package letisko.service;

import letisko.api.*;
import letisko.api.transformations.DestinaceConverter;
import letisko.beans.*;
import letisko.beans.api.CRUDBeanFactory;
import letisko.model.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import letisko.exception.EntityNotAvailableException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

public class LetiskoGenericServiceMockTest {
    private final List<CommonTO> tos = new ArrayList<>();
    private final List<CommonEntity> entities = new ArrayList<>();
    private LetiskoGenericService service = new LetiskoGenericService();

    @Before
    public void init() {
        CRUDBeanFactory crudBeanFactory = mock(CRUDBeanFactory.class);
        service.crudBeanFactory = crudBeanFactory;

        when(crudBeanFactory.getCRUDBean(any(Class.class))).then(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                Class<? extends CommonTO> toClass = (Class<? extends CommonTO>) invocationOnMock.getArguments()[0];
                CRUDBean bean = null;

                if (DestinaceTO.class.isAssignableFrom(toClass)) {
                    bean = mockCrudBean(DestinaceDAO.class, DestinaceTO.class, Destinace.class);
                } else if (LetadloTO.class.isAssignableFrom(toClass)) {
                    bean = mockCrudBean(LetadloDAO.class, LetadloTO.class, Letadlo.class);
                } else if (LetTO.class.isAssignableFrom(toClass)) {
                    bean = mockCrudBean(LetDAO.class, LetTO.class, Let.class);
                } else if (LetuskaTO.class.isAssignableFrom(toClass)) {
                    bean = mockCrudBean(LetuskaDAO.class, LetuskaTO.class, Letuska.class);
                }

                return bean;
            }
        });
    }

    private CRUDBean mockCrudBean(Class<? extends CRUDBean> daoClass, final Class<? extends CommonTO> toClass, final Class<? extends CommonEntity> entityClass) {
        CRUDBean bean;
        bean = mock(daoClass);
        when(bean.getTOClass()).thenReturn(toClass);
        when(bean.nacitaj(anyLong())).then(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                for (CommonTO to : tos) {
                    if (to.getClass().equals(toClass) && to.getId().equals(invocationOnMock.getArguments()[0])) {
                        return to;
                    }
                }
                return null;
            }
        });
        when(bean.vsetky()).then(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                List<CommonTO> ret = new ArrayList<>();
                for (CommonTO to : tos) {
                    if (to.getClass().equals(toClass)) {
                        ret.add(to);
                    }
                }
                return ret;
            }
        });
        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                Long id = new Random().nextLong();
                DestinaceTO to = (DestinaceTO) invocationOnMock.getArguments()[0];
                to.setId(id);
                Destinace entity = new DestinaceConverter().toEntity(to);
                entity.setId(id);
                tos.add(to);
                entities.add(entity);
                return null;
            }
        }).when(bean).vytvor(any(toClass));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                Long id = (Long) invocationOnMock.getArguments()[0];
                for (CommonTO to : tos) {
                    if (toClass.isAssignableFrom(to.getClass()) && to.getId().equals(id)) {
                        tos.remove(to);
                        break;
                    }
                }
                for (CommonEntity entity : entities) {
                    if (entityClass.isAssignableFrom(entity.getClass()) && entity.getId().equals(id)) {
                        entities.remove(entity);
                        break;
                    }
                }
                return null;
            }
        }).when(bean).zmaz(anyLong());
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                DestinaceTO arg = (DestinaceTO) invocationOnMock.getArguments()[0];
                for (CommonTO to : tos) {
                    if (toClass.isAssignableFrom(to.getClass()) && to.getId().equals(arg.getId())) {
                        tos.remove(to);
                        tos.add(arg);
                        break;
                    }
                }
                for (CommonEntity entity : entities) {
                    if (entityClass.isAssignableFrom(entity.getClass()) && entity.getId().equals(arg.getId())) {
                        entities.remove(entity);
                        entities.add(new DestinaceConverter().toEntity(arg));
                        break;
                    }
                }
                return null;
            }
        }).when(bean).uprav(any(DestinaceTO.class));
        return bean;
    }

    @Test
    public void testVytvorDestinaci() {
        assertEquals(Collections.emptyList(), service.vsetky(DestinaceTO.class));
        DestinaceTO destinaceTO = new DestinaceTO("SVK", "BA");
        try {
            service.vytvor(destinaceTO);
        } catch (EntityNotAvailableException ex) {
            fail();
        }
        assertEquals(1, service.vsetky(DestinaceTO.class).size());
        assertEquals(destinaceTO, service.vsetky(DestinaceTO.class).get(0));
    }
}
