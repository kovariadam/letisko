package letisko.beans;

import letisko.api.LetuskaTO;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Unit/Integracny test pre LetuskaDAO
 */
@RunWith(Arquillian.class)
public class LetuskaDAOTest extends CommonDAOTest {
    @Inject
    private LetuskaDAO letuskaDAO;

    @Test
    public void testVytvorAUpravLetusku() {
        LetuskaTO letuska = new LetuskaTO("Jana", "Mala");
        letuskaDAO.vytvor(letuska);

        List<LetuskaTO> letusky = letuskaDAO.vsetky();
        assertNotNull(letusky);
        assertEquals(1, letusky.size());
        assertEquals(letuska.getMeno(), letusky.get(0).getMeno());
        assertEquals(letuska.getPriezvisko(), letusky.get(0).getPriezvisko());

        letuska.setPriezvisko("Velka");
        letuskaDAO.uprav(letuska);

        letusky = letuskaDAO.vsetky();
        assertNotNull(letusky);
        assertEquals(1, letusky.size());
        assertEquals(letuska.getMeno(), letusky.get(0).getMeno());
        assertEquals(letuska.getPriezvisko(), letusky.get(0).getPriezvisko());
    }
}
