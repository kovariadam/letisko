package letisko.beans;

import letisko.api.DestinaceTO;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 * Unit/Integracny test pre DestinaceDAO
 */
@RunWith(Arquillian.class)
public class DestinaceDAOTest extends CommonDAOTest {
    @Inject
    private DestinaceDAO destinaceDAO;

    @Test
    public void testVytvorDestinaciu() {
        DestinaceTO nova = new DestinaceTO("Slovakia", "Bratislava");
        destinaceDAO.vytvor(nova);

        List<DestinaceTO> destinace = destinaceDAO.vsetky();
        assertNotNull(destinace);
        assertEquals(1, destinace.size());
        assertEquals(nova.getMesto(), destinace.get(0).getMesto());
        assertEquals(nova.getStat(), destinace.get(0).getStat());
    }

    @Test
    public void testZmazDestinaciu() {
        DestinaceTO nova = new DestinaceTO("Slovakia", "Bratislava");
        destinaceDAO.vytvor(nova);
        List<DestinaceTO> destinace = destinaceDAO.vsetky();

        assertEquals(1, destinace.size());

        destinaceDAO.zmaz(destinace.get(0).getId());
        destinace = destinaceDAO.vsetky();

        assertEquals(0, destinace.size());
    }
}
