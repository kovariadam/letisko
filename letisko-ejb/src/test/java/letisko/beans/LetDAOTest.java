package letisko.beans;

import java.util.ArrayList;
import letisko.api.DestinaceTO;
import letisko.api.LetTO;
import letisko.api.LetadloTO;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import java.util.Date;
import java.util.List;
import letisko.api.LetuskaTO;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Unit/Integracny test pre LetDAO
 */
@RunWith(Arquillian.class)
public class LetDAOTest extends CommonDAOTest {
    @Inject
    private LetDAO letDAO;
    @Inject
    private DestinaceDAO destinaceDAO;
    @Inject
    private LetadloDAO letadloDAO;
    @Inject
    private LetuskaDAO letuskaDAO;
    
    @Test
    public void testVytvorLet() {
        LetuskaTO letuska1 = new LetuskaTO("Milka","Sucha");
        LetuskaTO letuska2 = new LetuskaTO("Janka", "Pekna");
        letuskaDAO.vytvor(letuska1);
        letuskaDAO.vytvor(letuska2);
        List<LetuskaTO> letusky = new ArrayList<>(2);
        letusky.add(letuska1);
        letusky.add(letuska2);
        
        DestinaceTO destinaceTO1 = new DestinaceTO("Bulgaria", "Burgas");
        DestinaceTO destinaceTO2 = new DestinaceTO("Slovakia", "Bratislava");
        destinaceDAO.vytvor(destinaceTO1);
        destinaceDAO.vytvor(destinaceTO2);
        
        LetadloTO letadloTO = new LetadloTO(100, "Letadlo1", "Boeing373");
        letadloDAO.vytvor(letadloTO);
        
        LetTO novy = new LetTO(letadloTO,destinaceTO1,destinaceTO1, new Date(2013, 12, 12, 20, 20),new Date(2013, 12, 12, 22, 20),letusky);
        letDAO.vytvor(novy);
        LetTO novy2 = new LetTO(letadloTO,destinaceTO1,destinaceTO1, new Date(2013, 1, 12, 20, 20),new Date(2013, 1, 12, 22, 20),letusky);
        letDAO.vytvor(novy2);
        
        List<LetTO> lety = letDAO.vsetky();
        assertNotNull(lety);
        assertEquals(2, lety.size());
        assertEquals(novy.getLetadlo().getNazov(), lety.get(0).getLetadlo().getNazov());
        assertEquals(novy.getOdlet(), lety.get(0).getOdlet());
        assertEquals(novy.getPrilet(), lety.get(0).getPrilet());
        assertEquals(novy.getDestinaceStart().getMesto(), lety.get(0).getDestinaceStart().getMesto());
        assertEquals(novy.getDestinaceCil().getMesto(), lety.get(0).getDestinaceCil().getMesto());
        assertEquals(novy.getLetusky().get(0).toString(),lety.get(0).getLetusky().get(0).toString());
        
        LetadloTO letadloTO2 = new LetadloTO(333, "Emirates01", "Boeing 747");
        letadloDAO.vytvor(letadloTO2);
        
        DestinaceTO destinaceTO3 = new DestinaceTO("India", "New Delhi");
        destinaceDAO.vytvor(destinaceTO3);
        
        novy.setLetadlo(letadloTO2);
        novy.setDestinaceCil(destinaceTO3);
        letDAO.uprav(novy);

        lety = letDAO.vsetky();
        assertNotNull(lety);
        assertEquals(2, lety.size());
        assertEquals(novy.getLetadlo().getNazov(), lety.get(0).getLetadlo().getNazov());
        assertEquals(novy.getDestinaceCil().getMesto(), lety.get(0).getDestinaceCil().getMesto());
    }
}
