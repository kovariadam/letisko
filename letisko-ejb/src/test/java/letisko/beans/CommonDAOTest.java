package letisko.beans;

import letisko.api.LetuskaTO;
import letisko.api.transformations.TypeConverter;
import letisko.model.Letuska;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.After;
import org.junit.Before;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.*;
import java.io.File;

/**
 * Spolocna abstraktna trieda pre integracne testy
 */
public abstract class CommonDAOTest {
    @PersistenceContext
    protected EntityManager em;
    @Inject
    protected UserTransaction tx;

    @Deployment
    public static WebArchive createDeployment() {
        File[] libs = Maven.resolver()
                .loadPomFromFile("pom.xml").resolve("commons-beanutils:commons-beanutils")
                .withTransitivity().as(File.class);

        return ShrinkWrap.create(WebArchive.class)
                .addPackage(Letuska.class.getPackage())
                .addPackage(LetuskaTO.class.getPackage())
                .addPackage(LetuskaDAO.class.getPackage())
                .addPackage(TypeConverter.class.getPackage())
                .addAsLibraries(libs)
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("resources-jbossas-managed/test-persistence.xml", "META-INF/persistence.xml");
    }

    @Before
    public void begin() {
        try {
            tx.begin();
        } catch (NotSupportedException e) {
            e.printStackTrace();
        } catch (SystemException e) {
            e.printStackTrace();
        }
    }

    @After
    public void commit() {
        try {
            tx.commit();
        } catch (javax.transaction.RollbackException e) {
            e.printStackTrace();
        } catch (HeuristicMixedException e) {
            e.printStackTrace();
        } catch (HeuristicRollbackException e) {
            e.printStackTrace();
        } catch (SystemException e) {
            e.printStackTrace();
        }
    }
}
