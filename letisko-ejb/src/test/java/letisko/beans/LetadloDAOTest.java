package letisko.beans;

import letisko.api.LetadloTO;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Unit/Integracny test pre LetadloDAO
 */
@RunWith(Arquillian.class)
public class LetadloDAOTest extends CommonDAOTest {
    @Inject
    private LetadloDAO letadloDAO;

    @Test
    public void testVytvorLetadlo() {
        LetadloTO nova = new LetadloTO(100, "Boeing", "787");
        letadloDAO.vytvor(nova);

        List<LetadloTO> destinace = letadloDAO.vsetky();
        assertNotNull(destinace);
        assertEquals(1, destinace.size());
        assertEquals(nova.getNazov(), destinace.get(0).getNazov());
        assertEquals(nova.getTyp(), destinace.get(0).getTyp());
    }

    @Test
    public void testZmazLietadlo() {
        LetadloTO nova = new LetadloTO(100, "Boeing", "787");
        letadloDAO.vytvor(nova);
        List<LetadloTO> destinace = letadloDAO.vsetky();

        assertEquals(1, destinace.size());

        letadloDAO.zmaz(destinace.get(0).getId());
        destinace = letadloDAO.vsetky();

        assertEquals(0, destinace.size());
    }
}